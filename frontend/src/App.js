import React, {Component} from 'react';
import axios from 'axios';

// import logo from './logo.svg';
// import './App.css';

// mock data
// const list = [
//   {
//     "id":1,
//     "title":"1st todo",
//     "body":"Learn Django Properly."
//   },
//   {
//     "id":2,
//     "title":"Second item",
//     "body":"Learn Python."
//   },
//   {
//     "id":3,
//     "title":"Learn HTTP",
//     "body":"It's important."
//   }
// ];


class App extends Component{
  state = {
    todos: []
  };

  componentDidMount() {
    this.getTodos();
  }

  getTodos(){
    axios
        .get('http://127.0.0.1:8000/api/')
        .then(res => {
          this.setState({todos: res.data});
        })
        .catch(err => {
          console.log(err)
    })
  }

  // for mock data
  // constructor(props) {
  //   super(props);
  //   this.state={list};
  // }

  render() {
    return (
        <div>
          {this.state.todos.map(item => (
              <div key={item.id}>
                <h1>{item.title}</h1>
                <p>{item.body}</p>
              </div>
          ))}
        </div>
    );
  };
}

// function App() {
//   return (
//     <div className="App">
//       <header className="App-header">
//         <img src={logo} className="App-logo" alt="logo" />
//         <p>
//           Edit <code>src/App.js</code> and save to reload.
//         </p>
//         <a
//           className="App-link"
//           href="https://reactjs.org"
//           target="_blank"
//           rel="noopener noreferrer"
//         >
//           Learn React
//         </a>
//       </header>
//     </div>
//   );
// }

export default App;
