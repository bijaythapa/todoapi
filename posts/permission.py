from rest_framework import permissions


class IsAuthorOrReadOnly(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        # Read-only permissions are allowed for any requests
        if request.method in permissions.SAFE_METHODS:
            return True
        return obj.author == request.user

# class IsAdminUserOrReadOnly(permissions.BasePermission):
#     def has_object_permission(self, request, view, obj):
#         # Read-only permissions are allowed for all requests
#         if request.method in permissions.SAFE_METHODS:
#             return True
#
#         # if obj.is_staff == request.user:
#         #     return obj.author == request.user
#
#         return obj.is_staff == request.user
