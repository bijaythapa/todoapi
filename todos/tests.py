from django.test import TestCase
from .models import ToDo


class TodoModelTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        ToDo.objects.create(title="first todo", body="a body here")

    def test_title_content(self):
        todo = ToDo.objects.get(id=1)
        expected_object_name = f'{todo.title}'
        self.assertEqual(expected_object_name, 'first todo')

    def test_body_content(self):
        todo = ToDo.objects.get(id=1)
        # expected_object_name = f'{todo.body}'
        self.assertEqual(todo.body, 'a body here')
